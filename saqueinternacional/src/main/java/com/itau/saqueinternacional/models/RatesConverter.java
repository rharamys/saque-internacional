package com.itau.saqueinternacional.models;

import java.util.HashMap;

import org.springframework.web.client.RestTemplate;

public class RatesConverter {

	static RestTemplate restTemplate = new RestTemplate();
	
	public int timestamp;
	
	public static double convert(String from, String to, double amount) {
		
		double valor = 0 ;
		
		
		valor = Double.parseDouble(getMoedas().get(to)) * amount;
			
		return valor;
	}
	
	public static HashMap<String, String>  getMoedas()
	{
		
		return getResponse().getRates();
	}
	
	public static RatesResponse getResponse() {
		
		String url = "http://data.fixer.io/api/latest?access_key=e539e11f4de63138df2cea4a75a4ad58";
		
		RatesResponse rates = new RatesResponse();
		
		rates = restTemplate.getForObject(url, RatesResponse.class);
		
		return rates;
		
	}

	
}
