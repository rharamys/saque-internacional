package com.itau.saqueinternacional.controllers;

import java.util.HashMap;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.itau.saqueinternacional.models.Customer;
import com.itau.saqueinternacional.models.WithdrawRequest;
import com.itau.saqueinternacional.repositories.CustomerRepository;
import com.itau.saqueinternacional.services.RatesConverter;

@RestController
public class CustomerController {

	@Autowired
	CustomerRepository customerRepository;
	
	@Autowired
	RatesConverter ratesConverter;
	
	@RequestMapping(method=RequestMethod.POST, path="/saque")
	public ResponseEntity<Customer> withdraw(@RequestBody WithdrawRequest withdrawRequest) {
		Optional<Customer> optionalCustomer = customerRepository.findByUsername(withdrawRequest.getUsername());
		if (!optionalCustomer.isPresent()) {
			return ResponseEntity.status(404).build();
		}
		
		Customer customer = optionalCustomer.get();
		double convertedAmount = ratesConverter.convert("USD", withdrawRequest.getCurrency(), withdrawRequest.getAmount());
		if(convertedAmount > customer.getBalance())
		{
			return ResponseEntity.status(400).build();
		}
		customer.setBalance(customer.getBalance()-convertedAmount);
		customerRepository.save(customer);
		return ResponseEntity.ok().body(customer);
	}
	
	@RequestMapping(method=RequestMethod.GET, path="/customer/{id}")
	public ResponseEntity<Customer> getCustomer(@PathVariable long id) {
		Optional<Customer> optionalCustomer = customerRepository.findById(id);
		if (!optionalCustomer.isPresent()) {
			return ResponseEntity.status(404).build();
		}
		return ResponseEntity.ok().body(optionalCustomer.get());
	}
	
	@RequestMapping(method=RequestMethod.POST, path="/customer")
	public ResponseEntity<Customer> postCustomer(@Valid @RequestBody Customer customer) {
		try {
			return ResponseEntity.ok().body(customerRepository.save(customer));
		} catch (Exception e) {
			return ResponseEntity.badRequest().build();
		}
	}
	
	@RequestMapping(method=RequestMethod.GET, path="/customers")
	public Iterable<Customer> getCustomers() {
		return customerRepository.findAll();
	}
	
	@RequestMapping(method=RequestMethod.GET, path="/moedas")
	public HashMap getMoedas() {
		return ratesConverter.getMoedas();
	}
}
