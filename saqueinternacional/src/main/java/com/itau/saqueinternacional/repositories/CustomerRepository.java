package com.itau.saqueinternacional.repositories;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.itau.saqueinternacional.models.Customer;

public interface CustomerRepository extends CrudRepository<Customer, Long> {

	public Optional<Customer> findByUsername(String username);
	
}
